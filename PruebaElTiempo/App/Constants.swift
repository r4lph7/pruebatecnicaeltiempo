//
//  Constants.swift
//  PruebaElTiempo
//
//  Created by RalphM on 7/08/21.
//

import Foundation


struct Constants {
    static let apiURL = "https://images-api.nasa.gov/search?q="
    static let defaultSearch = "Apollo+11"
}

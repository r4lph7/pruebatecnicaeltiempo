//
//  CommonUtil.swift
//  PruebaElTiempo
//
//  Created by RalphM on 7/08/21.
//

import UIKit

class CommonUtil: NSObject {
    
    static let loading = Loading.instanceFromNib() as? Loading
    
    class func showAlert(_ alertTitle: String, alertContent: String, fromViewController: UIViewController, actionTitle: String) {
        let alert = UIAlertController(title: alertTitle, message: alertContent, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: actionTitle, style: UIAlertAction.Style.default, handler: nil))
        fromViewController.present(alert, animated: true, completion: nil)
    }
    
    class func addLoading(from:UIView){
        loading?.frame = CGRect(x: from.bounds.midX - loading!.bounds.midX,y: from.bounds.midY - loading!.bounds.midY, width: loading!.bounds.width,height: loading!.bounds.height)
        from.addSubview(loading!)
    }
    
    class func removeLoading(){
        loading?.removeFromSuperview()
    }
    
    class func configureTop(navigationItem: UINavigationItem){
        let imageView = UIImageView.init(image: UIImage.init(named: "logo"))
        imageView.contentMode = .scaleAspectFit
        let titleView = UIView.init(frame: CGRect(x: 0, y: 0, width: 100, height: 26))
        imageView.frame = titleView.bounds
        titleView.addSubview(imageView)
        navigationItem.titleView = titleView;
    }
}

//
//  PostController.swift
//  PruebaElTiempo
//
//  Created by RalphM on 7/08/21.
//

import UIKit

class PostController: UIViewController {
    
    @IBOutlet weak var postDescription : UITextView!
    @IBOutlet weak var postTitle: UILabel!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var dotStart: UIButton!
    
    var postViewModel = PostViewModel.shared
    var post : Post?

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        CommonUtil.configureTop(navigationItem: navigationItem)
    }
    
    func setupView(){
        if let post = post {
            dotStart.setImage(UIImage.init(systemName: (post.isFavorite) ? "star.fill" : "star"), for: .normal)
            postTitle.text = post.data.first?.title
            postDescription.text = post.data.first?.descr
            postDescription.sizeToFit()
            
            guard let imageURLString = post.links?.first?.href.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed),
                  let imageURL = URL(string: imageURLString) else { return }
            image.kf.setImage(with: imageURL)
        }
    }
    
    @IBAction func setFavorite(){
        guard var post = post else { return }
        post.isFavorite.toggle()
        dotStart.setImage(UIImage.init(systemName: post.isFavorite ? "star.fill" : "star"), for: .normal)
        postViewModel.setFavorite(post: post.managedObject(), favorite: post.isFavorite)
    }

}

//
//  PostViewModel.swift
//  PruebaElTiempo
//
//  Created by RalphM on 6/08/21.
//

import Foundation


class PostViewModel {
    static let shared = PostViewModel()
    
    func setFavorite(post: PostObject, favorite:Bool) {
        API.postStore.setFavorite(post, isFavorite: favorite)
    }
}



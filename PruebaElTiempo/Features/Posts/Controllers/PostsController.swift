//
//  ViewController.swift
//  PruebaElTiempo
//
//  Created by RalphM on 7/08/21.
//

import UIKit

class PostsController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchField: SearchField!
    
    private let postsViewModel = PostsViewModel.shared
    var selectedPost : Post!
    var dataSource : [Post]! = [] {
        didSet {
            tableView.reloadData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        postsViewModel.getPosts()
        if let search = searchField {
            postsViewModel.filterPosts(filterString: search.text ?? "")
        }
        CommonUtil.configureTop(navigationItem: navigationItem)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        configBindings()
        setupView()
    }
    
    func setupView(){
        let nib = UINib.init(nibName: "PostCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "PostCell")
    }
    
    func configBindings() {
        postsViewModel.onDidLoadPosts = {
            self.dataSource = self.postsViewModel.currentPosts
        }
        postsViewModel.onDidFailLoad = { error in
            CommonUtil.showAlert("Error", alertContent: "Error cargando el contenido", fromViewController: self, actionTitle: "OK")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detail" {
            let postController = segue.destination as? PostController
            postController?.post = selectedPost
        }
    }

}

extension PostsController :  UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedPost = (tableView.cellForRow(at: indexPath) as? PostCell)?.post
        self.performSegue(withIdentifier: "detail", sender: self)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension PostsController :  UITableViewDataSource{
 
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 284
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PostCell", for: indexPath) as! PostCell
        cell.post = dataSource[indexPath.row]
        return cell
    }
    
}

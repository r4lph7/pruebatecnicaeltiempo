//
//  PostsViewModel.swift
//  PruebaElTiempo
//
//  Created by RalphM on 7/08/21.
//

import Foundation


class PostsViewModel {
    private(set) var posts: [Post] = []
    private(set) var currentPosts: [Post] = []
    var onDidLoadPosts: (() -> Void)?
    var onDidFailLoad : ((_ error:String) -> Void)?
    static let shared = PostsViewModel()
    
    func getPosts(){
        API.retrievePosts() { [weak self] (posts) in
            guard let strongSelf = self else { return }
            strongSelf.posts = posts ?? []
            strongSelf.currentPosts = strongSelf.posts
            if strongSelf.currentPosts.count == 0 {
                strongSelf.onDidFailLoad?("No Posts")
            } else {
                strongSelf.onDidLoadPosts?()
            }
        }
    }
    
    func filterPosts(filterString: String){
        if filterString != "" {
            currentPosts = posts.filter {
                return $0.data.first?.title.lowercased().contains(filterString.lowercased()) ?? false
            }
        }else{
            currentPosts = posts
        }
        onDidLoadPosts?()
    }
}

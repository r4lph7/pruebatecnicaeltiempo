//
//  PostCell.swift
//  PruebaElTiempo
//
//  Created by RalphM on 7/08/21.
//

import UIKit
import Kingfisher

class PostCell: UITableViewCell {
    
    @IBOutlet weak var container : UIView!
    @IBOutlet weak var nasaImage : UIImageView!
    @IBOutlet weak var title : UILabel!
    @IBOutlet weak var dotStart : UIButton!
    
    var postViewModel = PostViewModel.shared
    
    var post : Post! {
        didSet {
            if let post = post {
                title.text = post.data.first?.title ?? ""
                dotStart.setImage(UIImage.init(systemName: (post.isFavorite) ? "star.fill" : "star"), for: .normal)
                guard let imageURLString = post.links?.first?.href.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed),
                      let imageURL = URL(string: imageURLString) else { return }
                nasaImage.kf.setImage(with: imageURL)
            }
        }
    }
    
    @IBAction func setTavorite(sender: UIButton){
        post.isFavorite.toggle()
        dotStart.setImage(UIImage.init(systemName: post.isFavorite ? "star.fill" : "star"), for: .normal)
        postViewModel.setFavorite(post: post.managedObject(), favorite: post.isFavorite)
    }
    
    override func prepareForReuse() {
        title.text = ""
        nasaImage.image = UIImage.init()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        container.layer.cornerRadius = 20
        container.layer.masksToBounds = true
    }
}

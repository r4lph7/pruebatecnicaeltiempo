//
//  Post.swift
//  PruebaElTiempo
//
//  Created by RalphM on 7/08/21.
//

import Foundation
import RealmSwift

struct Post : Codable, Identifiable, Equatable {
    
    let id : String
    var data : [Data]
    var links : [Link]?
    var isFavorite : Bool = false
    
    enum CodingKeys: String, CodingKey {
        case id = "href"
        case data
        case links
    }
    
    static func ==(lhs: Post, rhs: Post) -> Bool {
        return lhs.id == rhs.id
    }
}

class Link : Object, Codable {
    @Persisted var href : String
}

class Data : Object, Codable {
    @Persisted var media_type: String
    @Persisted var date_created: String
    @Persisted var title: String
    @Persisted var descr: String
    @Persisted var keywords: List<String>
    @Persisted var nasa_id: String?
    @Persisted var center: String?
    @Persisted var location: String?
    @Persisted var photographer: String?
    
    enum CodingKeys: String, CodingKey {
        case media_type
        case date_created
        case descr = "description"
        case title
        case keywords
        case nasa_id
        case center
        case location
        case photographer
    }
}


// MARK: - Realm Post
internal final class PostObject: Object {
    @Persisted var id = ""
    @Persisted var data = List<Data>()
    @Persisted var links = List<Link>()
    @Persisted var isFavorite = false
    
    override static func primaryKey() -> String? {
        return "id"
    }
}

// MARK: - Post Persistance
extension Post: Persistable {
    public init(managedObject: PostObject) {
        id = managedObject.id
        data = Array(managedObject.data)
        links = Array(managedObject.links)
        isFavorite = managedObject.isFavorite
    }
    
    public func managedObject() -> PostObject {
        let postObject = PostObject()
        postObject.id = id
        postObject.data.append(objectsIn: data)
        postObject.links.append(objectsIn: links ?? [])
        postObject.isFavorite = isFavorite
        return postObject
    }
}



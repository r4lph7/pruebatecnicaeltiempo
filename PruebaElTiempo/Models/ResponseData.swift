//
//  ResponseData.swift
//  PruebaElTiempo
//
//  Created by RalphM on 7/08/21.
//

import Foundation

struct ResponseData : Codable {
    
    struct Collection : Codable {
        var version: String
        var items : [Post]
        var href: String
        var links : [Link]?
        var metadata : Metadata
    }
    
    struct Metadata : Codable {
        var total_hits : Int
    }
    
    struct Link : Codable {
        var rel: String?
        var prompt: String?
        var href: String?
    }

    var collection: Collection
}


//
//  API.swift
//  PruebaElTiempo
//
//  Created by RalphM on 7/08/21.
//

import Foundation
import Alamofire
import RealmSwift

typealias APICallback = (ResponseData?) -> ()
typealias PostsCallback = ([Post]?) -> ()

protocol APIProtocol {
    static func retrievePosts(completionHandler: @escaping (PostsCallback))
}

struct API : APIProtocol {
    
    static let postStore = PostStore()
    static private let apiURL = Constants.apiURL
    static private let defaultSearch = Constants.defaultSearch
    static private func getHeaders() -> HTTPHeaders {
        return ["content-type": "application/json"]
    }
    
    static func retrievePosts(completionHandler: @escaping (PostsCallback)) {
        postStore.loadStoredPosts { storedPosts in
            if let posts = storedPosts, posts.count > 0 {
                completionHandler(posts)
            } else {
                retrievePostsFromAPI() { data in
                    guard let apiData = data else {
                        completionHandler(nil)
                        return
                    }
                    storePosts(posts: apiData.collection.items)
                    completionHandler(apiData.collection.items)
                }
            }
        }
    }
}

extension API {
    private static func retrievePostsFromAPI(completionHandler: @escaping (APICallback)){
        let headers: HTTPHeaders = self.getHeaders()
        let url = apiURL + defaultSearch
        AF.request(url, method: .get, headers:headers).response { response in
            DispatchQueue.main.async {
                switch response.result {
                    case .success:
                        guard let data = response.data,
                                let responseData = try? JSONDecoder().decode(ResponseData.self, from: data) else { break }
                        completionHandler(responseData)
                    case .failure(let error):
                        print(error.errorDescription ?? "")
                        completionHandler(nil)
                }
            }
        }
    }
    
    static func storePosts(posts: [Post]) {
        postStore.realm = try? Realm()
        postStore.savePosts(posts)
    }
}

//
//  Persistable.swift
//  PruebaElTiempo
//
//  Created by RalphM on 7/08/21.
//

import Foundation
import RealmSwift

public protocol Persistable {
    associatedtype ManagedObject: RealmSwift.Object
    init(managedObject: ManagedObject)
    func managedObject() -> ManagedObject
}

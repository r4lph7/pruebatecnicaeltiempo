//
//  PostStore.swift
//  PruebaElTiempo
//
//  Created by RalphM on 7/08/21.
//

import Foundation
import RealmSwift

enum RuntimeError: Error {
    case NoRealmSet
}

typealias DeleteHandler = (Bool) -> Void

class PostStore {
    var realm: Realm?
    
    private func savePost(_ post: PostObject) throws {
        if realm != nil {
            try? realm?.write {
                realm?.add(post, update:.all)
            }
        } else {
            throw RuntimeError.NoRealmSet
        }
        
    }
    
    public func savePosts(_ posts: [Post]) {
        do {
            try posts.forEach({ post in
                do {
                    try savePost(post.managedObject())
                } catch RuntimeError.NoRealmSet {
                    print(RuntimeError.NoRealmSet.localizedDescription)
                }
            })
        } catch {
            print("Unexpected error")
        }
    }
    
    private func retrievePosts() throws -> Results<PostObject> {
        if let realm = realm {
            return realm.objects(PostObject.self)
        } else {
            throw RuntimeError.NoRealmSet
        }
    }
    
    private func retrievePost(_ href: String) throws -> Results<PostObject> {
        if let realm = realm {
            return realm.objects(PostObject.self).filter("id = %@", href)
        } else {
            throw RuntimeError.NoRealmSet
        }
    }
    
    public func loadStoredPosts(completionHandler: @escaping PostsCallback) {
        realm = try? Realm()
        
        guard let realmPosts = try? retrievePosts() else {
            completionHandler(nil)
            return
        }
        
        let posts = Array(realmPosts).map { Post(managedObject: $0) }
        completionHandler(posts)
    }
    
    public func setFavorite(_ post: PostObject, isFavorite: Bool) {
        realm = try? Realm()
        
        let posts = try? retrievePost(post.id)
        try? realm?.write {
            posts?.setValue(isFavorite, forKey: "isFavorite")
        }
    }
    
}

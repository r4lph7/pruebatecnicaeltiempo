//
//  PruebaElTiempoTests.swift
//  PruebaElTiempoTests
//
//  Created by RalphM on 7/08/21.
//

import XCTest
@testable import PruebaElTiempo

class PruebaElTiempoTests: XCTestCase {
    
    func testloadPosts() throws {
        
        let retrivePostsTest = expectation(description: "retrivePostsTest")
        
        API.retrievePosts() { posts in
            guard let posts = posts else { return }
            guard let post = posts.first else { return }
            XCTAssertTrue(posts.count > 99) // HAS ALL POSTS
            XCTAssertEqual(post.isFavorite, false) // No Favorite
            PostViewModel.shared.setFavorite(post: post.managedObject(), favorite: true) // Set Favorite
            retrivePostsTest.fulfill()
        }
        
        //PERSISTENCE
        API.retrievePosts { posts in
            guard let posts = posts else { return }
            guard let post = posts.first else { return }
            XCTAssertEqual(post.isFavorite, true) // Is now favorite
        }
        
        waitForExpectations(timeout: 10, handler: nil)
    }

}
